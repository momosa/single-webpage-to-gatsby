import React from "react"
import styled from "styled-components"
import { StaticImage } from "gatsby-plugin-image"

const GlobalStyle = styled.div`
  /* Place your global css here */

  p {
    font-size: large;
  }

  /* Start with your old code.
   * Later, use styled components instead of classes.
   */
  .red {
    color: red;
  }
`

const BlueParagraph = styled.p`
  color: blue;
`

export default function HomePage() {
  return (
    <GlobalStyle>

      <p>
        Place your html here.
      </p>

      <p className="red">
        Initially, just change "class" to "className".
        Later, switch to styled components.
      </p>

      <BlueParagraph>
        This is a styled paragraph.
      </BlueParagraph>

      <p>
        Here's an OG image element.
      </p>

      <img
        alt="Bumi, Chief Treat Officer of Open Social Resources"
        src="images/bumi.jpg"
      />

      <p>
        Take advantage of Gatsby's image optimization.
        Use the following example.
        See the <a
          href="https://www.gatsbyjs.com/plugins/gatsby-plugin-image/"
          target="_blank" rel="noreferrer"
        >Gatsby docs</a> for more details.
      </p>

      <StaticImage
        alt="Bumi, Chief Treat Officer of Open Social Resources"
        src="../images/bumi.jpg"
      />

    </GlobalStyle>
  )
}
