# Convert a basic single webpage to React+Gatsby

```
HTML/CSS

↓ ↓ ↓

React+Gatsby
```

Use this repo to convert a basic single webpage to Gatsby.
A *basic single webpage* is just HTML and CSS,
like one you'd build in a Free Code Camp project
on those topics.

This project was created by
[Open Social Resources][osr_website]
as a learning tool to help new devs
ramp up on React and Gatsby,
given that they've already learned the basics of HTML and CSS.

- - -

## Contents

1. [Preliminaries](#preliminaries)

    1.1. [Prerequisite knowledge](#prerequisite_knowledge)

    1.2. [System setup](#system_setup)

    1.3. [Serving websites](#serving_websites)

1. [Convert your page to React+Gatsby](#convert_to_gatsby)

    2.1. [Initial migration](#initial_migration)

    2.2. [Enhanced migration](#enhanced_migration)

1. [What we accomplished, and what's next](#next_steps)
1. [Project setup notes](#from_scratch)


- - -

<a name="preliminaries"></a>

## 1\. Preliminaries

<a name="prerequisite_knowledge"></a>

### 1.1\. Prerequisite knowledge

- [ ] Basic HTML/CSS.
  You've made significant headway in the Free Code Camp
  HTML/CSS course, or equivalent.
- [ ] Basic CLI.
  You can navigate directories and
  input the commands provided in these instructions.
  There are illustrations to help with relevant
  parts of the directory structure.
- [ ] Basic reading.
  There are steps to take and links to documentation.
- [ ] Cursory knowledge of React,
  like that it involves components, whatever those are.
- [ ] Cursory knowledge of Gatsby,
  like that it combines React with
  SSR (Server-Side Rendering) to make super fast websites.

You're *not* expected to know about
serving websites and website publication.
There's [a section below](#serving_websites)
that will show you the basics.

<a name="system_setup"></a>

### 1.2\. System setup

The following assumes you're using Linux/OSX.

#### Step 1: Install the Node Package Manager (NPM)

See [the npm docs][install_npm]
for instructions on installing npm on your system.

##### Verify your NPM installation:

```shell
which npm
```

If it outputs npm's locatoin,
you're in good shape.
If it doesn't output anything,
something is wrong.

#### Step 2: Install `gatsby-cli`

```shell
npm install -g gatsby-cli
```

That will install the `gatsby` command line tool,
allowing you to issue commands using `gatsby`.

##### Verify your Gatsby installation:

```shell
which gatsby
```

If it outputs Gatsby's location,
you're in good shape.
If it doesn't output anything,
something is wrong.

#### Step 3: Install project packages

From the project directory:

```shell
npm install
```

##### Verify this installation

```shell
gatsby develop
```

That should kick off a development server at `localhost:8000`.
Visit `http://localhost:8000` in your browser to check it out.

<a name="serving_websites"></a>

### 1.3\. Serving websites

This section covers the basics of serving a production-level website.

#### Crash course

You've created HTML/CSS in files and opened those pages in your browser.
From there, you're able to see how your site looks,
inspect the DOM, etc.
You can change your code and refresh the page for quick feedback.
It's a nice process, once you get used to it.

Then you come across an even slightly more advanced project like this one.
*Where's index.html?*
*How do I see the site in my browser?*

As your website grows and involves more advanced technology,
it's a good idea to take extra steps for optimization and security.
This holds no matter what framework you're using,
be it React+Gatsby or just plain JavaScript.
Fortunately, there's some consistency in the new approach:

![Basics of serving a website: dev vs prod servers][image_serving]

Like the good 'ol days,
you want to be able to open up your site in a browser.
From there, you want to
adjust your code, refresh your browser, rinse and repeat.
That's what the development server is for.
You get all that familiar functionality and more.
But instead of using your browser to open a file,
you start the server and go to `localhost:8000` or something similar.

To allow you to see your changes quickly,
the dev server isn't optimized.
Optimization can make a big difference,
expecially mobile users and users with slower connections.
The website you deploy in production should be optimized.
Optimization includes processes like code minification,
server-side rendering, image optimization, etc.
The build step handles that process.

The commands to build optimized content and spin up servers
are pretty streamlined.
We'll cover those in the next section.

In summary:

- **Dev server**:
  See the website in your browser.
  View code changes quickly.
- **Build process**:
  Optimizes your content.
- **Prod server**:
  See the website in your browser.
  Displays optimized content.
  Requires a `build` to reflect code changes.

#### Gatsby commands

Here are a few useful commands.
Give them a try now and as you integrate your code.

```shell
# See an overview of gatsby commands.
gatsby --help

# Run a development server.
# Input http://localhost:8000/ into your browser to see your site.
# The dev server will automatically update as you change your code,
# although you may need to disable your browser's cache to see the changes.
gatsby develop

# Build your website for production.
# Minifies code, optimizes images, handles SSR, etc.
gatsby build

# Run a production server.
# You need to run gatsby build, first.
# Input http://localhost:9000/ into your browser to see your site.
# The prod server will NOT automatically update as you change your code.
gatsby serve
# Combo build and serve:
gatsby build && gatsby serve

# Clear cached files.
# This is rarely necessary,
# but useful if you want to start your build from scratch.
gatsby clean
```

If you want to see the website from other devices on your network,
like your phone or another computer,
add `--host 0.0.0.0` when running a dev or prod server.

```shell
gatsby develop --host 0.0.0.0
gatsby serve --host 0.0.0.0
```

When you run those commands,
your output should include something like the following.
Note the `On Your Network` portion,
which is what the `--host 0.0.0.0` adds.

```
You can now view singe-webpage-to-gatsby in the browser.
⠀
  Local:            http://localhost:8000/
  On Your Network:  http://192.168.1.123:8000/
```

The IP address may vary in the later digits,
depending on your computer's address in your local network.

For example, suppose I run a dev server on my computer
using:

```shell
gatsby develop --host 0.0.0.0
```

The output includes the `On Your Network`
section indicated above.
My computer and phone are both on my home network
via my router.
So I can input `http://192.168.1.123:8000/`
into my phone's browser to see the website.
Don't forget that the IP address may be different for you,
and the port 8000 will become 9000 if you're running
a prod server instead of a dev server.
Keep your eyes open for those differences.

Please practice a little with the `gatsby` commands given above.
We'll use most of them a lot in the following sections.

**NOTE**:
If you're used to running commands directly with `npm`,
like `npm build`,
those are set up for you.
See the `scripts` section in `package.json` for a full list.


- - -

<a name="convert_to_gatsby"></a>

## 2\. Convert your page to React+Gatsby

Before proceeding,
ensure you can run the server before inputting your code.
See the instructions in the previous section.

<a name="initial_migration"></a>

### 2.1\. Initial migration

The first thing we want to do is place your content onto the page.

Open the following file in your favorite IDE or text editor,
along with the content you want to convert.

```
src/pages/index.jsx
```

Gatsby will turn that into your `index.html`.
We want substitute the starter code in that `index.jsx`
with your own HTML and CSS.

- [ ] Migrate your CSS.
- [ ] Insert your HTML.
- [ ] Change `class` to `className`.
- [ ] Change HTML comments to React comments.
- [ ] Set up `img` elements.

#### Step 1: Migrate your CSS

In `src/pages/index.jsx`, you'll see:

```js
const GlobalStyle = styled.div`
  /* Place your global css here */
  ...
```

Just replace the `...` with your CSS.

#### Step 2: Insert your HTML

The `HomePage` definition looks like:

```js
export default function HomePage() {
  return (
    <GlobalStyle>

      ...

    </GlobalStyle>
  )
}
```

First, consider commenting out the existing content
instead of deleting it.
Recall that in React, comments are enclosed in `{/* comment */}`
The existing content demonstrates a couple of things you may need later.
Second, place your HTML body
(the html under the `<body>` tag) within `<GlobalStyle>`.
The result should have the following structure:

```js
export default function HomePage() {
  return (
    <GlobalStyle>

      Your HTML body content

      {/*
        Old stuff
      */}

    </GlobalStyle>
  )
}
```

#### Step 3: Change `class` to `className`

React uses `className` instead of `class`.
You might be able to just do a find/replace for:
`class=` → `className=`.

**NOTE**:
As long as you have your CSS defined in `GlobalStyle`,
all content under `GlobalStyle` inherits those styles.
Among other things, all of the elements under `GlobalStyle`
can use classes defined there.

#### Step 4: Change HTML comments to React comments

You may have some HTML comments in your content.
React-style comments are a little different.

- HTML comment: `<!-- ... -->`
- React comment: `{/* ... */}`

Multi-line comments work as expected, too.

If you use HTML-style comments,
you'll need to convert them to React-style comments.

#### Step 5: Set up `img` elements

Start with OG `img` elements.
There's a sample `img` in `/src/pages/index.jsx`:

```html
<img
  alt="Bumi, Chief Treat Officer of Open Social Resources"
  src="images/bumi.jpg"
/>
```

TL;DR place your images in `static/images/` and refer to them as above.
Read on for details.

That `img` element should look nice and familiar.
But where is `images/bumi.jpg`?
Recall that `src/pages/index.jsx` becomes
`http://localhost:8000/index.html`
(or equivalent, depending on where it's served from)
The server will look for `images/bumi.jpg` in the same relative location as `index.html`.
So we need `http://localhost:8000/images/bumi.jpg`.
How do we tell Gatsby to put it there?

Put `images/bumi.jpg` in the `static/` folder.

```
static
├── favicon.ico
└── images
    └── bumi.jpg
src
├── images
│   └── bumi.jpg
└── pages
    └── index.jsx
```

Ignore `src/images/bumi.jpg` for now.
That one is for Gatsby to handle images efficiently.
We'll get to it later.

Anything in the static folder is available at the root url of the site.

```
src/static/images/bumi.jpg

↓ ↓ ↓

http://localhost:8000/images/bumi.jpg
```

Kick off a dev server and paste that URL in your browser.

And there it is.
The `static/` folder is where you can place files like
`robots.txt`, `sitemap.xml`, and `favicon.ico`.
`static/` is also a convenient location for downloadable files like PDFs
because you get clean and consistent URLs.

We're going to use `static/images/` for intermediate image handling
using OG `img` elements.
Once you place your images there,
it's easy to refer to them in the `src` attribute of `img` elements.

<a name="enhanced_migration"></a>

### 2.2\. Enhanced migration

- [ ] Use Gatsby images
- [ ] Use components

#### Step 1: Use Gatsby images

Gatsby handles images very efficiently,
but the syntax is a little different from
HTML or even vanilla React.
The upshot is that Gatsby will make your website
a whole lot faster.

There are two places to note in `src/pages/index.jsx`.
The first is where we import the `StaticImage` component
near the top of the file.

```js
import { StaticImage } from "gatsby-plugin-image"
```

Second, the page content has an example of `StaticImage`:

```html
<StaticImage
  alt="Bumi, Chief Treat Officer of Open Social Resources"
  src="../images/bumi.jpg"
/>
```

This looks a lot like a familiar `img` element.

Note the location of that image file, in `src/images/`:

```
src
├── images
│   └── bumi.jpg
└── pages
    └── index.jsx
```

Just place your images in `src/images/` and
proceed to migrate your `img` elements to `StaticImage` components.
Once you've eliminated all `img` elements,
feel free to remove any now-unused images in `static/images/`.

In some cases, Gatsby images behave differently than OG `img` elements.
If your content isn't displayed as expected,
the first thing to try is placing the image inside a `div`,
then adjusting the styling of that `div`.
(You can create an `ImageContainer` styled component.)
Check out [Gatsby's documentation on handling images][gatsby_plugin_image]
to learn more about image handling in Gatsby.
There's a very helpful community online if get stuck.

**WARNING**:
If you're looking into Gatsby images online,
focus on the `gatsby-plugin-image`,
not the obsolete `gatsby-image`.
The two are very different.

#### Step 2: Use components

For the purpose of this project,
migrating to components is typically going to involve the following transition:

```
HTML element with CSS class

↓ ↓ ↓

React styled component
```

Styled components are React components defined by their styles.
They can even take props and use variables.
Styled components have their limitations,
but they're a powerful tool and
a great way to start using components,
utilizing your knowledge of CSS.

As we proceed,
migrating your classes to components may appear silly,
especially once you see the example in `HomePage`.
It is.
You're working with a small web page that doesn't use JavaScript.
Components don't really shine here.
Trust that they do elsewhere.
This project allows you to focus on the basics,
which are still crucial for larger and more advanced projects.

There's a sample transition in `/src/pages/index.jsx`
using red and blue paragraphs.
The red one uses a class,
while the blue one uses a styled component.

The red paragraph uses the `red` class:

```html
<p className="red">
  Initially, just change "class" to "className".
  Later, switch to styled components.
</p>
```

The `red` class is defined in `GlobalStyle`:

```css
.red {
  color: red;
}
```

The blue paragraph is a styled component:

```html
<BlueParagraph>
  This is a styled paragraph.
</BlueParagraph>
```

`BlueParagraph` is defined in the same file, above the `HomePage`:

```js
const BlueParagraph = styled.p`
  color: blue;
`
```

Using this approach,
go ahead and replace all of your classes with styled components.
Instead of a bunch of `div`s and classes,
you should be left with informatively named components, like
`Header`, `HeroImage`, and so forth.

- - -

<a name="next_steps"></a>

## 3\. What we accomplished, and what's next

We've focused on transforming a single webpage that uses HTML/CSS
into one that uses components.
You should be more familiar with using components.
Your site should also be a lot faster,
especially on mobile devices.
(Sorry, we're not getting into performance analysis here.)

That's a great start,
but there's a lot more to learn.

Here are a couple of possible next steps:

- [ ] Integrate multiple pages.
      A `404` page would be a great place to start.
- [ ] Separate re-used components from page files.
- [ ] Improve metadata and SEO (Search Engine Optimization).
- [ ] Deploy to GitLab or Github pages.
- [ ] Do more cool stuff!

- - -

<a name="from_scratch"></a>

### 4\. Project setup notes

The purpose of this section is to
remove some of the mystery of how this repo was created.
This explanation is just to enhance your understanding.
You don't need to do anything.

*If you're using this repo,
you don't need to complete any of the following steps.*

- [ ] Create a basic Gatsby starter project.
- [ ] Install additional packages.
- [ ] Set up intermediate image handling using `img`.
- [ ] Create the initial homepage to
      make it easy to transition a webpage.

With that,
you can start placing content using
the process described in previous sections.

#### Step 1: Create a basic Gatsby starter project

Once you've installed `npm` and `gatsby-cli`,
it's time to create your project.
Suppose you want a project called `gatsby-starter`,
and you want that directory to be within `~/projects/`.
We'll create a version of Gatsby's "Hello World" starter
in a directory called `gatsby-starter`.

```shell
cd ~/projects/
gatsby new gatsby-starter https://github.com/gatsbyjs/gatsby-starter-hello-world
```

You should now have a directory `~/projects/gatsby-starter`
containing a "Hello World" starter.

See the [Gatsby Hello World Starter docs][gatsby_starter_hello_world]
for more details.

**NOTE**:
Installing a Gatsby starter as described above
will also install `README.md`, `LICENSE`,
as well as various files associated with git,
like `.gitignore` and `.git/`.
Consider deleting or modifying those as needed.
Personally, I deleted `README.md`, `LICENSE`, and `.git/`;
leaving `.gitignore`.

#### Step 2: Install additional packages

```shell
# Execute this command in the project dir.
npm install \
  gatsby-plugin-styled-components styled-components babel-plugin-styled-components \
  gatsby-source-filesystem \
  gatsby-plugin-image gatsby-plugin-sharp gatsby-transformer-sharp
```

- `npm install`:
  Install some node packages into your project.
  Once this is complete,
  your packages should be listed under `dependencies`
  in the `package.json` file,
  located in your project dir.
- `gatsby-plugin-styled-components styled-components babel-plugin-styled-components`:
  Packages for integrating CSS into components
  using `styled-components`.
  This will make it easier to use your knowledge of CSS
  to style your React components.
  Styled components also helps ease the transition from using a big block of global CSS
  hanging out in your HTML header
  to focusing on writing your clear styles for individual components.
  See the [Gatsby docs on styled components][gatsby_styled_components].
- `gatsby-source-filesystem`:
  Used for certain optimized interactions with your system
  and enabling some GraphQL functionality.
  We're going to tell this plugin where to find images
  so that Gatsby can optimize them during the SSR process.
- `gatsby-plugin-image gatsby-plugin-sharp gatsby-transformer-sharp`:
  Tools for using images in Gatsby.
  This will make our site super fast.
  See the [Gatsby docs on images][gatsby_plugin_image].

Gatsby plugins should be listed in `gatsby-config.js`,
where you can also adjust settings for those plugins.
Modify `gatsby-config.js` accordingly.
The final result should look something like:

```js
/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

module.exports = {
  plugins: [

    // Styled components for CSS-in-JS
    // https://www.gatsbyjs.com/docs/how-to/styling/styled-components/
    // https://styled-components.com/docs
    `gatsby-plugin-styled-components`,

    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images/`,
      },
    },


    // https://www.gatsbyjs.com/plugins/gatsby-plugin-image/
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,

  ],
}
```

Be sure to create `src/images/`
or `gatsby-source-filesystem` will lose its mind
when you try to build.
Consider putting something in there
if you need Git to pick up the directory.
Your project folder should contain
(image filename will vary):

```
src
├── images
│   └── bumi.jpg
└── pages
    └── index.jsx
```

#### Step 3: Set up intermediate image handling using `img`

This tutorial handles images in two steps.
First, use familiar `img` elements,
then transition those to more efficient Gatsby images.

To make it easy to set the `src` attribute of `img` elements,
we place images into `static/images/`.
You can create the directory `static/images/`
and place something in there
so that Git will pick up the directory.

```
static
├── favicon.ico
└── images
    └── bumi.jpg
```

#### Step 4: Create the initial homepage to make it easy to transition a webpage.

We get `src/pages/index.jsx` with the Gatsby "Hello World" starter,
but it's pretty bare-bones.
We can make it easier to place an existing HTML/CSS page into.
To start the transition process,
you need to be able to place two things:

1. Global CSS, and
1. HTML from the page's `body` element.

We'll use styled-components to handle the CSS.
Import it at the top of the file:

```js 
import styled from "styled-components"
```

Create a component to hold the styles:

```js
const GlobalStyle = styled.div`
  /* Place your global css here */
`
```

Finally, wrap the page content within those styles.
The HomePage component should look like:

```js
export default function HomePage() {
  return (
    <GlobalStyle>
      <p>Place your html here.</p>
    </GlobalStyle>
  )
}
```

Looks like we set up a place for the HTML along the way.
Nice work, us!

[image_serving]: unprocessed_media/serving.png "Serving a website"

[gatsby_cli]: https://www.gatsbyjs.com/docs/reference/gatsby-cli/
[gatsby_plugin_image]: https://www.gatsbyjs.com/plugins/gatsby-plugin-image/
[gatsby_starter_hello_world]: https://www.gatsbyjs.com/starters/gatsbyjs/gatsby-starter-hello-world
[gatsby_styled_components]: https://www.gatsbyjs.com/docs/how-to/styling/styled-components/
[install_npm]: https://docs.npmjs.com/cli/v8/configuring-npm/install
[osr_website]: https://opensocialresources.com/
